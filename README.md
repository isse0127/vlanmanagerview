このプロジェクトは [Create React App](https://github.com/facebook/create-react-app). を使用しています

## 環境構築

このプロジェクトは [Create React App](https://github.com/facebook/create-react-app). を使用しています

### インストール

vlanmanagerviewフォルダ直下で
`npm install`
を実行してください

### ローカル実行

`npm start`でreact-scriptsによりローカル起動します
起動portは`.env`を変更してください

## アーキテクチャ

### 状態管理

シンプルな画面・データ構造のため、各Componentでstateを管理しています
Flux、Reduxは使用していません
