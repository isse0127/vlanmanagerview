import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Label, Input, Form, FormGroup, } from "reactstrap";
import axiosBase from "axios";

// APIアクセス用の設定
const axios = axiosBase.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
    },
    responseType: "json",
    withCredentials: true
});
let groupInit = "";
let descriptionInit = "";

export default class GroupInfoModal extends React.Component {

    constructor() {
        super();

        this.state = {
            floor: "",
            group: "",
            description: "",
            groupAddMode: false,
            btnDisabled: true
        };
    }
    // Component開始処理
    componentDidMount() {
        this.init();
    }

    // Component終了処理 
    componentWillUnmount() {
    }

    // Component変更処理（条件違いの呼び出し）
    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            this.init();
        }
    }
    // apiError発生時の処理 以降の処理続けない場合にtrueを返す
    apiError(error) {
        // log出力
        console.log(error);
        // 認証エラーならlogin画面遷移
        if (error.response && error.response.status === 401) {
            window.location.href = "/login/?ru=" + window.location.pathname;
            return true;
        }
        return false;
    }

    // 画面のinit処理
    init() {
        this.setState({
            floor: this.props.floor,
            group: this.props.groupAddMode ? "" : this.props.group,
            description: this.props.description,
            groupAddMode: this.props.groupAddMode,
            modalOpen: false,
            btnDisabled: this.props.btnDisabled,
        });
        // 変更前の値を保持
        groupInit = this.props.group;
        descriptionInit = this.props.description;
    }

    handleChange(event) {
        switch (event.target.name) {
            case "group":
                this.setState({ group: event.target.value });
                break;
            case "description":
                this.setState({ description: event.target.value });
                break;
            default:
                console.log("key not found");
        }
    }
    handleFormSubmit(event) {
        event.preventDefault();

        if (this.state.group === "") {
            alert("グループ名を入力してください");
            return;
        }

        const apiBody = {
            name: this.state.group,
            description: this.state.description
        }

        let apiEndpoint;
        if (this.state.groupAddMode === true) {
            // api更新　/api/v1/floor/{floorName}/group
            apiEndpoint = "/api/v1/floor/" + this.props.floor + "/group";
            axios
                .post(apiEndpoint, apiBody)
                .then((results) => {
                    // propsの関数実行
                    this.props.returnFunc();
                    // updating終了
                    alert("グループ登録が完了しました");
                    // modal閉じる
                    this.setState({ modalOpen: false });
                    // 変更前の値を更新
                    groupInit = this.state.group;
                    descriptionInit = this.state.description;
                })
                .catch((error) => {
                    // error
                    if (this.apiError(error)) return;
                    alert("グループ登録に失敗しました");
                });
        } else {
            // api更新　/api/v1/floor/{floorName}/group/{groupName}
            apiEndpoint = "/api/v1/floor/" + this.props.floor + "/group/" + this.props.group;
            axios
                .put(apiEndpoint, apiBody)
                .then((results) => {
                    if (this.props.group === this.state.group) {
                        // propsの関数実行
                        this.props.returnFunc();
                        // updating終了
                        alert("説明の変更が完了しました");
                        // modal閉じる
                        this.setState({ modalOpen: false });
                        // 変更前の値を更新
                        groupInit = this.state.group;
                        descriptionInit = this.state.description;
                    } else {
                        // group変更時は画面再読み込み
                        alert("グループの変更が完了しました");
                        window.location.href = "/concentstatus/" + this.props.floor + "/" + this.state.group;
                    }
                })
                .catch((error) => {
                    // error
                    if (this.apiError(error)) return;
                    alert("グループ変更に失敗しました");
                });
        }

    }
    modalChange() {
        this.setState((state) => {
            // modalOpen,Closeのタイミングでgroup、descriptionは変更前または更新時の値に設定
            return { modalOpen: !state.modalOpen, group: groupInit, description: descriptionInit }
        });
    }
    render() {
        // 更新ボタンLabel
        const regBtnLable = this.state.groupAddMode ? "グループ登録" : "グループ変更";
        return (
            <div>
                <Button disabled={this.state.btnDisabled} color={this.props.btnColor} className={this.props.btnClass} onClick={() => { this.modalChange() }}>{this.props.btnLabel}</Button>
                <Modal size="lg" isOpen={this.state.modalOpen} toggle={() => { this.modalChange() }} >
                    <Form onSubmit={(e) => { this.handleFormSubmit(e) }}>
                        <ModalHeader toggle={() => { this.modalChange() }}>{this.state.floor}</ModalHeader>
                        <ModalBody>
                            <FormGroup>
                                <Label for="name">グループ名</Label>
                                <Input type="text" name="group"
                                    value={this.state.group} id="group" placeholder="グループ名" onChange={(e) => { this.handleChange(e) }} />
                                <Label for="description">説明</Label>
                                <Input type="textarea" rows="10" name="description" value={this.state.description}
                                    id="description" placeholder="説明" onChange={(e) => { this.handleChange(e) }} />
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <Button color="primary">{regBtnLable}</Button>
                            <Button color="primary" onClick={() => { this.modalChange() }}>閉じる</Button>
                        </ModalFooter>
                    </Form>
                </Modal>
            </div>
        );
    }
}