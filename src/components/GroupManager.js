import React from "react";
import axiosBase from "axios";
import { Button, CustomInput } from "reactstrap";
import "../css/GroupManager.css";
import Headerbar from "./Headerbar";
// APIアクセス用の設定
const axios = axiosBase.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
    },
    responseType: "json",
    withCredentials: true
});

// table行の色づけclass（bootstrap）
const trDefault = "table-light";
const trMod = "table-info";
const trError = "table-danger";

// 遷移param
let floor = "";
let group = "";


export default class GroupManager extends React.Component {
    constructor() {
        super();

        this.state = {
            concents:
                [
                    {
                        concentNo: "dummyconcentNo",
                        portNo: "dummyportNo",
                        groupJoin: false
                    }
                ]
        };
    }

    // Component開始処理
    componentDidMount() {
        this.initGroupManageres();
    }

    // Component終了処理 
    componentWillUnmount() {
    }

    // Component変更処理
    componentDidUpdate(prevProps) {
        // URLが違う
        if (this.props.match.params !== prevProps.match.params) {
            this.initGroupManageres();
        }
    }

    // apiError発生時の処理 以降の処理続けない場合にtrueを返す
    apiError(error) {
        // log出力
        console.log(error);
        // 認証エラーならlogin画面遷移
        if (error.response && error.response.status === 401) {
            window.location.href = "/login/?ru=" + window.location.pathname;
            return true;
        }
        return false;
    }
    // 画面のinit処理
    initGroupManageres() {
        floor = this.props.match.params.floor;
        group = this.props.match.params.group;
        this.setState({ concents: [] });
        this.getConcents();
    }
    // concents一覧の取得
    getConcents() {
        // /api/v1/floor/{floorName}/group/{groupName}/concents
        const apiEndpointGroup = "/api/v1/floor/" + floor + "/group/" + group + "/concents";
        // /api/v1/floor/{floorName}/concents
        const apiEndpointFloor = "/api/v1/floor/" + floor + "/concents";

        // groupのconcents取得=>floorのconcents取得=>floorとgroup比較
        axios
            .get(apiEndpointGroup)
            .then((groupResults) => {
                // respone
                // floorと比較のためString配列に変換
                // {concnetNo:"abc",portNo:"1"}=>"abc 1"
                const groupConcentsString = groupResults.data.concents
                    .map(concent => { return concent.concentNo + " " + concent.portNo; });
                // floorの全concents取得
                axios
                    .get(apiEndpointFloor)
                    .then((floorResults) => {
                        // respone
                        // floorのConcentがgroupに登録されているか判定
                        const floorConcents = floorResults.data.concents
                            .map((concent) => {
                                // groupにconcentがjoinしているか判定
                                if (groupConcentsString.indexOf(concent.concentNo + " " + concent.portNo) >= 0) {
                                    concent.groupJoin = true;
                                } else {
                                    concent.groupJoin = false;
                                }
                                concent.trClass = trDefault;
                                return concent;
                            });
                        this.setState({ concents: floorConcents });
                    },
                    )
                    .catch((error) => {
                        if (this.apiError(error)) return;
                        this.setState({ concents: [] });
                        alert("コンセントの情報取得に失敗しました");
                    });
            },
            )
            .catch((error) => {
                if (this.apiError(error)) return;
                this.setState({ concents: [] });
                alert("コンセントの情報取得に失敗しました");
            });
    }

    // groupJoin変更時の反映
    changeGroupJoin(index) {
        let concents = this.state.concents.slice();
        concents[index].groupJoin = !concents[index].groupJoin;
        this.setState({ concents: concents });
    }
    // groupJoinの一括変更
    allChangeGroupJoin(checked) {
        let concents = this.state.concents.slice();
        concents.forEach((concent, index) => {
            concents[index].groupJoin = checked;
        });
        this.setState({ concents: concents });
    }
    // 更新ボタン
    updateGroupJoin() {
        // api更新　/api/v1/floor/{floorName}/group/{groupName}/concents
        const apiEndpoint = "/api/v1/floor/" + floor + "/group/" + group + "/concents";
        const concents = this.state.concents.slice()
            .filter(concent => { return concent.groupJoin })
            .map(concent => {
                return { "concentNo": concent.concentNo, "portNo": concent.portNo };
            });
        const apiBody = { "concents": concents };
        axios
            .put(apiEndpoint, apiBody)
            .then((results) => {
                alert("グループを更新しました");
            })
            .catch((error) => {
                // error
                if (this.apiError(error)) return;
                alert("グループの更新に失敗しました");
            });
    }

    render() {
        return (
            <div id="content" className="d-flex flex-column">
                {/* Topbar */}
                <div className="header">
                    <Headerbar
                        floor={floor}
                        group={group}
                        groupMode={(true)}
                        loading={(false)} />
                </div>
                {/* Begin Page Content */}
                <div className="contents">
                    <div className="scroll">
                        <table className="table">
                            <thead>
                                <tr>
                                    <th width="40%">情報コンセント</th>
                                    <th width="10%">ポート</th>
                                    <th width="50%">
                                        <div>
                                            <Button className="btn-sm" color="light" onClick={(e) => { this.allChangeGroupJoin(true) }} >全てONにする</Button>
                                            <Button className="btn-sm ml-1" color="light" onClick={(e) => { this.allChangeGroupJoin(false) }} >全てOFFにする</Button>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody className="form-group">
                                {this.state.concents.map((concent, index) => {
                                    return (
                                        <tr key={concent.concentNo + concent.portNo} className={concent.trClass} >
                                            <td width="40%">{concent.concentNo}</td>
                                            <td width="10%">{concent.portNo}</td>
                                            <td width="50%">
                                                <CustomInput type="switch"
                                                    id={concent.concentNo + concent.portNo}
                                                    name="customSwitch"
                                                    checked={concent.groupJoin}
                                                    onChange={(e) => { this.changeGroupJoin(index) }} />
                                            </td>
                                        </tr>
                                    );
                                })}
                            </tbody >
                        </table>
                    </div>
                </div>
                {/* Bottombar */}

                <div className="modal-footer footer">
                    <Button color="primary" onClick={(e) => { this.updateGroupJoin() }} >更新</Button>
                </div>

            </div>

        );
    }
}

