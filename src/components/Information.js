import React from "react";

export default class Information extends React.Component {
    render() {
        return (
            <div id="content" className="d-flex align-items-center">
                <div className="mx-auto">
                    <div><img alt="VLANM TOOL" src="/logo.png" /></div>
                    <div>
                        <iframe width="800" height="500" title="information"
                            src="/information.html"></iframe>
                    </div>
                </div>
            </div>
        );
    }
}

