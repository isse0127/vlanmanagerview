import React from "react";
import { Navbar, Button, Collapse, CardBody, Card } from "reactstrap";
import Linkify from "react-linkify";
import GroupInfoModal from "./GroupInfoModal";
import { Link } from "react-router-dom";
import axiosBase from "axios";
// APIアクセス用の設定
const axios = axiosBase.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
    },
    responseType: "json",
    withCredentials: true
});

export default class Headerbar extends React.Component {

    constructor() {
        super();

        this.state = {
            floor: "",
            group: "",
            groupMode: false,
            descriptionOpen: true
        };
    }
    // Component開始処理
    componentDidMount() {
        this.init();
    }

    // Component終了処理 
    componentWillUnmount() {
    }

    // Component変更処理（条件違いの呼び出し）
    componentDidUpdate(prevProps) {
        if (this.props.floor !== prevProps.floor
            || this.props.group !== prevProps.group
            || this.props.loading !== prevProps.loading) {
            this.init();
        }
    }
    // apiError発生時の処理 以降の処理続けない場合にtrueを返す
    apiError(error) {
        // log出力
        console.log(error);
        // 認証エラーならlogin画面遷移
        if (error.response && error.response.status === 401) {
            window.location.href = "/login/?ru=" + window.location.pathname;
            return true;
        }
        return false;
    }

    // 画面のinit処理
    init() {
        this.setState({
            floor: this.props.floor,
            group: this.props.group,
            groupMode: this.props.groupMode,
            loading: this.props.loading,
            descriptionOpen: true
        });
        // 初期化時にfloor、groupが空で呼ばれるのでgroupModeでなくgroupで判定
        if (this.props.group) {
            this.getGroupInfo();
        }
    }
    // groupの取得
    getGroupInfo() {
        // api　/api/v1/floor/{floorName}/group/{groupName}
        const apiEndpoint = "/api/v1/floor/" + this.props.floor + "/group/" + this.props.group;
        axios
            .get(apiEndpoint)
            .then((results) => {
                this.setState({ description: results.data.description });
            })
            .catch((error) => {
                // error
                if (this.apiError(error)) return;
                this.setState({ description: "" });
                alert("グループの情報取得に失敗しました");
            });
    }

    render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <div className="ml-0" >{this.state.floor}</div>
                    {(this.state.groupMode) && (<div className="ml-3">{this.state.group}</div>)}
                    {(this.state.groupMode) && (< div className="ml-3">
                        <GroupInfoModal floor={this.state.floor} group={this.state.group} groupAddMode={false} description={this.state.description}
                            btnLabel="グループ変更" btnColor="primary" btnClass="btn-sm" btnDisabled={this.state.loading}
                            returnFunc={() => { this.getGroupInfo(); }} /></div>)}
                    {(!this.state.groupMode) && (< div className="ml-3">
                        <GroupInfoModal floor={this.state.floor} groupAddMode={true}
                            btnLabel="グループ登録" btnColor="primary" btnClass="btn-sm" btnDisabled={this.state.loading}
                            returnFunc={this.props.returnFuncGroupAdd} /></div>)}
                    {(this.state.groupMode) && (< div className="ml-3">
                        <Button color="primary" className="btn-sm"
                            onClick={() => { this.setState((state) => { return { descriptionOpen: !state.descriptionOpen } }) }}>説明</Button>
                    </div>)}
                    {(!this.state.groupMode) && (< div className="ml-3">
                        <Link to={"/audit/" + this.state.floor}>
                            <Button color="primary" className="btn-sm">History</Button>
                        </Link>
                    </div>)}
                    {(!this.state.groupMode) && (< div className="ml-3">
                        <a href={"/download?floor=" + this.state.floor}>
                            <Button color="primary" className="btn-sm">Csv</Button>
                        </a>
                    </div>)}
                </Navbar >
                {(this.state.groupMode) && (<div>
                    <Collapse isOpen={this.state.descriptionOpen}>
                        <Card>
                            <CardBody>
                                <Linkify>
                                    < div className="description">{this.state.description}</div>
                                </Linkify>
                            </CardBody>
                        </Card>
                    </Collapse>
                </div>)}
            </div >
        );
    }
}
