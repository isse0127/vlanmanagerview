import React from "react";
import { Navbar, Card, CardBody } from "reactstrap";
import axiosBase from "axios";
import "../css/Audit.css";

// APIアクセス用の設定
const axios = axiosBase.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest",
    },
    responseType: "json",
    withCredentials: true
});
const axios_text = axiosBase.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest",
    },
    responseType: "text",
    withCredentials: true
});

export default class Audit extends React.Component {
    constructor() {
        super();

        this.state = {
            loglist:
                [
                    {
                        name: ""
                    }
                ],
            log: ``
        };
    }
    // Component開始処理
    componentDidMount() {
        this.init();
    }

    // Component終了処理 
    componentWillUnmount() {
    }

    // Component変更処理
    componentDidUpdate(prevProps) {
        if (this.props.match.params !== prevProps.match.params) {
            this.init();
        }
    }
    init() {
        this.getLoglist();
    }
    // apiError発生時の処理 以降の処理続けない場合にtrueを返す
    apiError(error) {
        // log出力
        console.log(error);
        // 認証エラーならlogin画面遷移
        if (error.response && error.response.status === 401) {
            window.location.href = "/login/?ru=" + window.location.pathname;
            return true;
        }
        return false;
    }
    getLoglist() {
        // loglist取得　/api/v1/audit/{floorName}/list
        const apiEndpoint = "/api/v1/audit/" + this.props.match.params.floor + "/list";
        axios
            .get(apiEndpoint)
            .then((results) => {
                // respone
                this.setState({ loglist: results.data.loglist });
                // loglistがあれば先頭でログ表示
                this.setState((state) => {
                    if (state.loglist.length > 0) {
                        this.clickLoglist(state.loglist[0].name);
                    }
                });
            },
            )
            .catch((error) => {
                if (this.apiError(error)) return;
                this.setState({ loglist: [] });
                alert("ログの情報取得に失敗しました");
            });
    }
    clickLoglist(logName) {
        // log取得　/api/v1/audit/{floorName}/{logName}
        const apiEndpoint = "/api/v1/audit/" + this.props.match.params.floor + "/" + logName;
        axios_text
            .get(apiEndpoint)
            .then((results) => {
                // respone
                this.setState({ log: results.data });
            },
            )
            .catch((error) => {
                if (this.apiError(error)) return;
                this.setState({ log: `` });
                alert("ログの情報取得に失敗しました");
            });
    }
    render() {
        return (
            <div id="content" className="d-flex flex-column">
                {/* Topbar */}
                <div className="header">
                    <Navbar color="light" light expand="md">
                        <div className="ml-0" >{this.props.match.params.floor}</div>
                        <div className="ml-3" >
                            <select onChange={(e) => { this.clickLoglist(e.target.value) }}>
                                {this.state.loglist.map((log) => (
                                    <option key={log.name} value={log.name}>{log.name}</option>
                                ))}
                            </select>
                        </div>
                    </Navbar >
                </div>
                {/* Begin Page Content */}
                <div className="contents">
                    <div className="scroll">
                        <Card>
                            <CardBody>
                                < div >{this.state.log}</div>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </div>
        );
    }
}

