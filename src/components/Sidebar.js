import React from "react";
import { NavLink } from "react-router-dom";
import { Collapse } from "reactstrap";
import "../css/Sidebar.css";
import axiosBase from "axios";

// APIアクセス用の設定
const axios = axiosBase.create({
  baseURL: process.env.REACT_APP_API_ENDPOINT,
  headers: {
    "Content-Type": "application/json",
    "X-Requested-With": "XMLHttpRequest",
  },
  responseType: "json",
  withCredentials: true
});

export default class Sidebar extends React.Component {
  constructor() {
    super();

    this.state = {
      floors:
        [
          {
            name: "dummyfloor",
            groupToggle: false,
            groups:
              [
                {
                  name: "dummygroup",
                  description: "dummydescription"
                }
              ]
          }
        ],
      user: { name: "dummyname", mail: "dummymail" },
      contact: { link: "/" },
      reload: true //画面再描画用
    };
  }
  // Component開始処理
  componentDidMount() {
    this.initSidebar();
  }

  // Component終了処理 
  componentWillUnmount() {
  }

  // Component変更処理
  componentDidUpdate(prevProps) {
    if (this.props.sidebarReload !== prevProps.sidebarReload) {
      this.initSidebar();
    }
  }
  initSidebar() {
    this.getContact();
    this.getFloors();
    this.getUser();
  }
  // apiError発生時の処理 以降の処理続けない場合にtrueを返す
  apiError(error) {
    // log出力
    console.log(error);
    // 認証エラーならlogin画面遷移
    if (error.response && error.response.status === 401) {
      window.location.href = "/login/?ru=" + window.location.pathname;
      return true;
    }
    return false;
  }
  getContact() {
    // contact取得　/contact.json
    const apiEndpoint = "/contact.json";
    // server側で304が返ってきてaxiosで対応できないのでParamつけて取得
    const noCacheParam = "?timestamp=" + new Date().getTime();

    axios
      .get(apiEndpoint + noCacheParam)
      .then((results) => {
        // respone
        this.setState({ contact: results.data });
      },
      )
      .catch((error) => {
        if (this.apiError(error)) return;
        this.setState({ contact: { link: "/" } });
      });
  }
  getFloors() {
    // floor取得　/api/v1/floors
    const apiEndpoint = "/api/v1/floors";
    axios
      .get(apiEndpoint)
      .then((results) => {
        // respone
        const floors = results.data.floors
          .map((floor) => {
            floor.groupToggle = false;
            floor.groups = [];
            return floor;
          });
        this.setState({ floors: floors });
        // floorsセット後の同期処理
        this.setState((state) => {
          // URL直接指定時は該当のFLOORのToggleを開く
          const urlFloor = window.location.pathname.split("/")[2];
          state.floors.forEach((floor, index) => {
            if (floor.name === urlFloor) {
              this.clickFloorToggle(index);
            }
          });
        });
      },
      )
      .catch((error) => {
        if (this.apiError(error)) return;
        this.setState({ floors: [] });
        alert("フロアの情報取得に失敗しました");
      });
  }
  getUser() {
    // user取得　/api/v1/user
    const apiEndpoint = "/api/v1/user";
    axios
      .get(apiEndpoint)
      .then((results) => {
        // respone
        const user = results.data;
        this.setState({ user: user });
      })
      .catch((error) => {
        if (this.apiError(error)) return;
        alert("アカウントの情報取得に失敗しました");
      });
  }
  getNavLinkClass(floor, group) {
    // 想定URL
    // /concentstatus/:floor
    // /audit/:floor
    // /concentstatus/:floor/:group
    // /groupManager/:floor/:group

    // URLのPathをリスト取得
    const urlPathList = window.location.pathname.split("/");

    // floorのURL
    if (urlPathList.length === 3) {
      if (floor === urlPathList[2] && group === "") {
        return "active";
      }
    }
    // groupのURL
    if (urlPathList.length === 4) {
      if (floor === urlPathList[2] && group === urlPathList[3]) {
        return "active";
      }
    }
    // URLマッチしない
    return "";

  }
  clickFloorToggle(index) {
    // toggleを閉じる
    let floors = this.state.floors.slice()
      .map(floor => {
        floor.groupToggle = false;
        return floor;
      });
    // clickしたtoggleを開く
    floors[index].groupToggle = !floors[index].groupToggle;
    // floor取得　/api/v1/floor/{floorName}/groups
    const apiEndpoint = "/api/v1/floor/" + floors[index].name + "/groups";
    axios
      .get(apiEndpoint)
      .then((results) => {
        // respone
        floors[index].groups = results.data.groups;
        this.setState({ floors: floors });
      },
      )
      .catch((error) => {
        if (this.apiError(error)) return;
        this.setState({ floors: [] });
        alert("フロアの情報取得に失敗しました");
      });
  }
  render() {
    return (
      <nav id="sidebar" className="d-flex flex-column">
        <div className="sidebar-header header">
          <a href="/"><h6>VLAN Management Tool</h6></a>
        </div>
        <div className="contents">
          <ul className="list-unstyled components scroll">
            <p>{this.state.user.name}</p>
            <li className="active">
              <ul className=" list-unstyled" id="floorList">
                {this.state.floors.map((floor, index) => (
                  <li key={floor.name}>
                    <NavLink exact
                      onClick={(e) => { this.clickFloorToggle(index) }}
                      to={`/concentstatus/${floor.name}`}
                      className={this.getNavLinkClass(floor.name, "")}
                    >{floor.name}</NavLink>
                    <Collapse isOpen={floor.groupToggle}>
                      <ul className=" list-unstyled" id={floor.name + "_grouplist"}>
                        {floor.groups.map(group => (
                          <li key={floor.name + group.name}>
                            <NavLink exact
                              onClick={() => { this.setState((state) => { return { reload: !state.reload } }) }}
                              to={`/concentstatus/${floor.name}/${group.name}`}
                              className={this.getNavLinkClass(floor.name, group.name)}
                            >　{group.name}</NavLink>
                          </li>
                        ))}
                      </ul>
                    </Collapse>
                  </li>
                ))}
              </ul>
            </li>
          </ul>
        </div>
        <div className="footer">
          <div>ITS部IM課</div>
          <div>問い合わせ先は<a className="link" href={this.state.contact.link} rel="noreferrer noopener" target="_blank">こちら</a></div>
        </div>
      </nav>
    );
  }
}

