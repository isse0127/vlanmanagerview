import React from "react";
import { Link } from "react-router-dom";
import axiosBase from "axios";
import Headerbar from "./Headerbar";
import { Spinner, Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import "../css/ConcentStatus.css";
// APIアクセス用の設定
const axios = axiosBase.create({
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    headers: {
        "Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
    },
    responseType: "json",
    withCredentials: true
});

// table行の色づけclass（bootstrap）
const trDefault = "table-light";
const trMod = "table-info";
const trError = "table-danger";

// 遷移param
let floor = "";
let group = "";


export default class ConcentStatus extends React.Component {
    constructor() {
        super();

        this.state = {
            concents:
                [
                    {
                        concentNo: "dummyconcentNo",
                        portNo: "dummyportNo",
                        vlanId: 1,
                        trClass: "",
                        originVlanId: "dummyvlanId",
                        description: ``,
                        updating: false
                    }
                ],
            vlans:
                [
                    { vlanId: 1, name: "dummyvlan" }
                ],
            vlanIdDropdownToggle: false,
            loading: true
        };
    }

    // Component開始処理
    componentDidMount() {
        this.initConcentStatuses();
    }

    // Component終了処理 
    componentWillUnmount() {
    }

    // Component変更処理（Sidebarからの条件違いの呼び出し）
    componentDidUpdate(prevProps) {
        // URLが違う
        if (this.props.match.params !== prevProps.match.params) {
            this.initConcentStatuses();
        }
    }
    // apiError発生時の処理 以降の処理続けない場合にtrueを返す
    apiError(error) {
        // log出力
        console.log(error);
        // 認証エラーならlogin画面遷移
        if (error.response && error.response.status === 401) {
            window.location.href = "/login/?ru=" + window.location.pathname;
            return true;
        }
        return false;
    }
    // 画面のinit処理
    initConcentStatuses() {
        // floor指定の遷移ならgroupを初期化
        if (typeof this.props.match.params.group === "undefined") {
            floor = this.props.match.params.floor;
            group = "";
        } else {
            floor = this.props.match.params.floor;
            group = this.props.match.params.group;
        }
        this.setState({ loading: true, concents: [] })
        this.getVlans();
        this.getConcentStatuses();
        this.modCount = 0;
    }
    // concents一覧の取得
    getConcentStatuses() {
        let apiPath;
        if (group === "") {
            apiPath = "floor/" + floor;
        } else {
            apiPath = "floor/" + floor + "/group/" + group;
        }
        const apiEndpoint = "/api/v1/" + apiPath + "/concentStatuses";
        const apiStartPoint = floor + ":" + group;
        axios
            .get(apiEndpoint)
            .then((results) => {
                // respone
                const concents = results.data.concentStatuses
                    .map((concentStatus) => {
                        concentStatus.trId = concentStatus.concentNo + concentStatus.portNo;
                        concentStatus.trClass = trDefault;
                        concentStatus.originVlanId = concentStatus.vlanId;
                        concentStatus.updating = false;
                        return concentStatus;
                    });
                // 画面読込中にsidebarで別のfloorまたはgroupがクリックされた対応
                // API開始時と同じ画面の場合のみconcentsを設定
                if (apiStartPoint === floor + ":" + group) {
                    this.setState({ loading: false, concents: concents });
                }
            },
            )
            .catch((error) => {
                if (this.apiError(error)) return;
                if (apiStartPoint === floor + ":" + group) {
                    this.setState({ loading: false });
                }
                this.setState({ concents: [] });
                alert("コンセントの情報取得に失敗しました");
            });
    }
    // vlans一覧の取得
    getVlans() {
        // /api/v1/floor/{floorName}/vlans
        const apiEndpoint = "/api/v1/floor/" + floor + "/vlans";

        axios
            .get(apiEndpoint)
            .then((results) => {
                // respone
                const vlans = results.data.vlans;
                // viewStatusは未設定
                this.setState({ vlans: vlans });
            },
            )
            .catch((error) => {
                if (this.apiError(error)) return;
                this.setState({ vlans: [] });
                alert("コンセントの情報取得に失敗しました");
            }
            );
    }
    // vlanId変更時のselect反映
    changeVlanId(index, targetVlanId) {

        let concents = this.state.concents.slice();
        concents[index].vlanId = parseInt(targetVlanId, 10);

        // 取得時の値に戻した
        if (concents[index].vlanId === concents[index].originVlanId) {
            concents[index].trClass = trDefault;
            this.modCount = this.modCount - 1;
            // 取得時から初変更
        } else if (concents[index].trClass === trDefault) {
            concents[index].trClass = trMod;
            this.modCount = this.modCount + 1;
        } else {
            concents[index].trClass = trMod;
        }
        this.setState({ concents: concents });
    }
    // vlanIdの一括変更
    allChangeVlanId(targetVlanId) {
        this.state.concents.slice()
            .forEach((concent, index) => {
                this.changeVlanId(index, targetVlanId);
            });
    }

    // 更新ボタンClick処理
    updateVlanId(index) {

        let concents = this.state.concents.slice();
        // 該当行を処理中に
        concents[index].updating = true;
        this.setState({ concents: concents });

        // api更新　/api/v1/floor/{floorName}/concentStatus/{concentNo}/{portNo}
        const apiEndpoint = "/api/v1/floor/" + floor + "/concentStatus/"
            + concents[index].concentNo + "/" + concents[index].portNo;
        const apiBody = {
            concentNo: concents[index].concentNo,
            portNo: concents[index].portNo,
            vlanId: concents[index].vlanId
        }
        axios
            .put(apiEndpoint, apiBody)
            .then((results) => {
                this.setResponse(results.data.concentStatuses);
            })
            .catch((error) => {
                // error
                if (this.apiError(error)) return;
                concents[index].updating = false;
                concents[index].trClass = trError;
                this.setState({ concents: concents });
                alert(concents[index].concentNo + "-" + concents[index].portNo + "の更新に失敗しました");
            });
    }
    // APIの戻り値を反映する
    setResponse(apiResults) {
        const concents = this.state.concents.slice()
            .map(concent => {
                apiResults
                    .forEach((result) => {
                        // resultに該当あればresultの内容で更新
                        if (result.concentNo === concent.concentNo && result.portNo === concent.portNo) {
                            concent.updating = false;
                            concent.trClass = trDefault;
                            concent.originVlanId = result.vlanId;
                            concent.vlanId = result.vlanId;
                        }
                    });
                return concent;
            });
        this.setState({ concents: concents });
    }
    // 一括更新ボタンClick処理
    bulkUpdateVlanId() {
        this.setState({ loading: true });
        // api更新　/api/v1/floor/{floorName}/concentStatus
        const apiEndpoint = "/api/v1/floor/" + floor + "/concentStatuses";
        const concentStatuses = this.state.concents.slice()
            .filter(concent => {
                // 変更のclassが当たっていれば変更対象
                return concent.trClass === trMod
            })
            .map(concent => {
                return { concentNo: concent.concentNo, portNo: concent.portNo, vlanId: concent.vlanId };
            });
        const apiBody = { concentStatuses: concentStatuses };
        axios
            .put(apiEndpoint, apiBody)
            .then((results) => {
                this.setResponse(results.data.concentStatuses);
                this.setState({ loading: false });
            })
            .catch((error) => {
                // error
                if (this.apiError(error)) return;
                this.setState({ loading: false });
                alert("一括更新に失敗しました");
            });

    }
    // Group削除ボタンClick処理
    deleteGroup() {
        // api更新　/api/v1/floor/{floorName}/group/{groupName}
        const apiEndpoint = "/api/v1/floor/" + floor + "/group/" + group;
        axios
            .delete(apiEndpoint)
            .then((results) => {
                alert("グループを削除しました");
                window.location.href = "/concentstatus/" + floor;
            })
            .catch((error) => {
                // error
                if (this.apiError(error)) return;
                alert("グループの削除に失敗しました");
            });
    }
    render() {

        // vlanIdのoption作成
        const vlanOptions = this.state.vlans.map((vlan) => {
            return (<option key={vlan.vlanId} value={vlan.vlanId}>{vlan.name}</option>);
        });
        // vlanIdのoption作成
        const vlanAllSetOptions = this.state.vlans.map((vlan) => {
            return (<DropdownItem key={"vlanall_" + vlan.vlanId} onClick={() => { this.allChangeVlanId(vlan.vlanId) }}>{vlan.name}</DropdownItem>);
        });

        return (
            <div id="content" className="d-flex flex-column">
                {/* Topbar */}
                <div className="header">
                    <Headerbar
                        floor={floor}
                        group={group}
                        groupMode={(group !== "")}
                        loading={this.state.loading}
                        returnFuncGroupAdd={this.props.sidebarReload}
                    />
                </div>
                {/* Begin Page Content */}
                <div className="contents">

                    {this.state.loading ?
                        (
                            <div className="spinner d-flex">
                                <div className="mx-auto">
                                    <Spinner style={{ width: '5rem', height: '5rem' }} color="primary" />
                                </div>
                            </div>
                        ) :
                        (
                            <div className="scroll">
                                <table className="table">
                                    <thead>
                                        <tr>
                                            <th>情報コンセント</th>
                                            <th>ポート</th>
                                            <th>接続先</th>
                                            <th>
                                                <Dropdown size="sm" isOpen={this.state.vlanIdDropdownToggle}
                                                    toggle={() => { this.setState((state) => { return { vlanIdDropdownToggle: !state.vlanIdDropdownToggle } }) }}>
                                                    <DropdownToggle color="light" caret>
                                                        全ての接続先を変更
                                                </DropdownToggle>
                                                    <DropdownMenu>
                                                        {vlanAllSetOptions}
                                                    </DropdownMenu>
                                                </Dropdown>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody className="form-group">
                                        {this.state.concents.map((concent, index) => {
                                            return (
                                                <tr key={concent.trId} className={concent.trClass} >
                                                    <td>{concent.concentNo}</td>
                                                    <td>{concent.portNo}</td>
                                                    <td>
                                                        <select name="vlanId" value={concent.vlanId} className="form-control" onChange={(event) => { this.changeVlanId(index, event.target.value) }}>
                                                            {vlanOptions}
                                                        </select>
                                                    </td>
                                                    <td>
                                                        {concent.updating ?
                                                            (<Spinner color="primary" />) :
                                                            (<Button color="primary" onClick={(e) => { this.updateVlanId(index) }} >更新</Button>)
                                                        }
                                                    </td>
                                                </tr>
                                            );
                                        })}
                                    </tbody >
                                </table>
                            </div>
                        )
                    }
                </div>
                {/* Bottombar */}

                <div className="modal-footer footer">
                    {(group !== "") && (<Button disabled={this.state.loading} color="primary" onClick={(e) => { this.deleteGroup() }} >グループ削除</Button>)}
                    {(group !== "") && (<Link to={`/groupManager/${floor}/${group}`} ><Button disabled={this.state.loading} color="primary">グループ編集</Button></Link>)}
                    <Button disabled={this.state.loading} color="primary" onClick={(e) => { this.bulkUpdateVlanId() }} >一括更新</Button>
                </div>
            </div>

        );
    }
}

