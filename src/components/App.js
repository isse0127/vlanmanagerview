import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Sidebar from "./Sidebar";
import Information from "./Information";
import ConcentStatus from "./ConcentStatus";
import GroupManager from "./GroupManager";
import Audit from "./Audit";


export default class App extends React.Component {
  constructor() {
    super();

    this.state = {
      sidebarReload: true
    };
  }
  render() {
    return (
      <div className="wrapper">
        <Router >
          <Sidebar sidebarReload={this.state.sidebarReload} />
          <Switch>
            <Route exact path="/" component={Information} />
            <Route exact path="/concentstatus/:floor"
              render={props => <ConcentStatus
                sidebarReload={() => { this.setState((state) => { return { sidebarReload: !state.sidebarReload } }) }}
                {...props} />} />
            <Route exact path="/concentstatus/:floor/:group" component={ConcentStatus} />
            <Route exact path="/groupManager/:floor/:group" component={GroupManager} />
            <Route exact path="/audit/:floor" component={Audit} />
          </Switch>
        </Router>
      </div>
    );
  }
}

